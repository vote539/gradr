/*
 * File: app/store/GroupScores.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Gradr.store.GroupScores', {
    extend: 'Ext.data.Store',

    requires: [
        'Gradr.model.GroupScore'
    ],

    config: {
        autoLoad: true,
        model: 'Gradr.model.GroupScore',
        storeId: 'GroupScores',
        proxy: {
            type: 'ajax',
            url: 'server/?uri=gs',
            reader: {
                type: 'json'
            },
            writer: {
                type: 'json'
            }
        },
        listeners: [
            {
                fn: 'onGroupScoresWrite',
                event: 'write'
            }
        ]
    },

    onGroupScoresWrite: function(store, operation, eOpts) {
        // reload the filter
        State.setGroup(State.group);
    }

});