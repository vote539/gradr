(function(window){
	window.State = state = {
		module: null,
		setModule: function(module){
			if (!module) return;
			state.module = module;
			var groups = Ext.getStore("Groups");
			groups.clearFilter();
			groups.filter("module_number", module.get("number"));
			var rubrics = Ext.getStore("Rubrics");
			rubrics.clearFilter();
			rubrics.filter("module_number", State.module.get("number"));
		},
		showModule: function(applicationView){
			applicationView.push(Ext.create("Gradr.view.ApplicationStartView"));
		},
		createAndShowModule: function(number, applicationView){
			// Client-side validations should be OK since the application is
			// behind an authentication layer in which all users are personally
			// approved to use the application.
			if (!number) {
				alert("Please enter a module number");
				return;
			}
			var modules = Ext.getStore("Modules");
			if (modules.find("number", number) !== -1) {
				alert("A module with that number already exists");
				return;
			}
			var module = Ext.create("Gradr.model.Module", {
				number: number
			});
			modules.add(module);
			modules.sync();

			state.setModule(module);
			state.showModuleAdmin(applicationView);
		},
		showModuleAdmin: function(applicationView){
			applicationView.push(Ext.create("Gradr.view.ModuleAdmin"));
		},
		
		group: null,
		createAndShowGroup: function(applicationView){
			state.group = Ext.create("Gradr.model.Group", {
				module_number: state.module.get("number")
			});
			Ext.getStore("Groups").add(state.group);
			state.showGroup(applicationView);
		},
		setGroup: function(group){
			if (!group) return;
			state.group = group;
			Ext.getStore("StudentsInCurrentGroup").reload();
			var groupScores = Ext.getStore("GroupScores");
			groupScores.clearFilter();
			groupScores.filter("group_id", group.get("id"));
		},
		showGroup: function(applicationView){
			applicationView.push(Ext.create("Gradr.view.GroupView"));
		},
		showGradeGroup: function(applicationView){
			state.showGradeForm(false, applicationView);
		},
		updateGroupSubmissionDate: function(newDate){
			state.group.set("recorded", newDate);
			console.log("updateGroupSubmissionDate");
			Ext.getStore("Groups").sync();
		},
		
		showStudentChooser: function(applicationView){
			applicationView.push(Ext.create("Gradr.view.StudentChooser"));
		},
		addStudent: function(student){
			var newSG = Ext.create("Gradr.model.StudentsInGroups", {
				group_id: state.group.get("id"),
				student_id: student.get("id")
			});
			Ext.getStore("StudentsInGroups").add(newSG);
			Ext.getStore("StudentsInGroups").sync();
			Ext.getStore("StudentsInCurrentGroup").add(student);
		},
		
		student: null,
		setStudent: function(student){
			if (!student) return;
			state.student = student;
			var studentScores = Ext.getStore("StudentScores");
			studentScores.clearFilter();
			studentScores.filter("student_id", student.get("id"));
		},
		showGradeStudent: function(applicationView){
			state.showGradeForm(true, applicationView);
		},
		
		showGradeForm: function(isStudent, applicationView){
			var rubrics = Ext.getStore("Rubrics");
			var model = isStudent ? state.student : state.group;
			var store = isStudent ? Ext.getStore("StudentScores") : Ext.getStore("GroupScores");
			var formContainer = isStudent ?
			Ext.create("Gradr.view.GradeStudentContainer") : Ext.create("Gradr.view.GradeGroupContainer");
			var fieldsetContainer = formContainer.down("#GradeFieldsetContainer");
			applicationView.push(formContainer);
			rubrics.clearFilter();
			rubrics.filter([
				{ property: "module_number", value: State.module.get("number") },
				{ property: "for_group", value: !isStudent }
				]);
			rubrics.each(function(item, index, length){
				// Make a data view for each item
				var scoreRecord = model.getScoreRecord(item);
				var itemFormView = Ext.create("Gradr.view.RangeGrade", {
					title: item.get("description") + " (" + item.get("weight") + " pts)",
					instructions: item.get("special_instructions"),
					startValue: scoreRecord.get("integer_value"),
					maxValue: item.get("weight"),
					record: item
				});
				fieldsetContainer.add(itemFormView);
			});
			store.sync();
			this.updateTotalScore(isStudent, formContainer);
		},
		saveGrade: function(rangeGrade, rubric, newValue){
			var isStudent = !rubric.get("for_group");
			var model = isStudent ? state.student : state.group;
			var store = isStudent ? Ext.getStore("StudentScores") : Ext.getStore("GroupScores");
			var scoreRecord = model.getScoreRecord(rubric);
			var container = isStudent ? rangeGrade.up("#GradeStudentContainer") : rangeGrade.up("#GradeGroupContainer");
			
			scoreRecord.set("integer_value", newValue);
			store.sync();
			this.updateTotalScore(isStudent, container);
		},
		updateTotalScore: function(isStudent, container){
			State.setStudent(State.student); // reload the filter
			var totalScoreElem = container.down("#TotalScore");
			var store = isStudent ? Ext.getStore("StudentScores") : Ext.getStore("GroupScores");
			var prefix = isStudent ? "Individual Score" : "Group Score";
			var total = 0;
			store.each(function(item, index, length){
				total += item.get("integer_value");
			});
			totalScoreElem.setTitle(prefix + ": " + total);
		},

		rubric: null,
		setRubric: function(rubric){
			if (!rubric) return;
			if (rubric === true) {
				// make a new rubric
				state.rubric = Ext.create("Gradr.model.Rubric", {
					type: "range",
					module_number: State.module.get("number")
				});
			}else{
				// use an existing rubric
				state.rubric = rubric;
			}
		},
		showRubricAdmin: function(applicationView){
			var rubricAdminView = Ext.create("Gradr.view.RubricAdmin");
			var form = rubricAdminView.down("#RubricAdminFormPanel");
			applicationView.push(rubricAdminView);
			form.setRecord(State.rubric);
		},
		saveRubric: function(form){
			var rubric = State.rubric;
			form.updateRecord(rubric);
			var rubrics = Ext.getStore("Rubrics");
			if (rubrics.indexOf(rubric) === -1) {
				rubrics.add(rubric);
			}
			rubrics.sync();
		},
		importRubrics: function(dataString){
			var rawModuleStrings = dataString.split("\n");
			var newModules = [];
			rawModuleStrings.each(function(moduleString){
				// Skip entry if empty
				if (/^\s*$/.test(moduleString)) return;

				var moduleInfo = moduleString.split(";");
				moduleInfo = moduleInfo.map(function(str){
					return str.trim();
				});

				var module = Ext.create("Gradr.model.Rubric", {
					type: "range",
					module_number: State.module.get("number"),
					for_group: moduleInfo[0] === "g",
					weight: parseInt(moduleInfo[1]),
					description: moduleInfo[2] || "",
					special_instructions: moduleInfo[3] || ""
				});
				Ext.getStore("Rubrics").add(module);

			});
			Ext.getStore("Rubrics").sync();
		},

		exportModuleData: function(){
			var rubrics = Ext.getStore("Rubrics"),
				students = Ext.getStore("Students"),
				studentScores = Ext.getStore("StudentScores"),
				groupScores = Ext.getStore("GroupScores"),
				csvData = [];

			// Make the header row
			var csvRubrics = [["Student ID", "Name"], [], ["Group Date"], []];
			rubrics.data.items.each(function(rubric){
				var destCsv = rubric.get("description");
				if (rubric.get("for_group")) {
					csvRubrics[3] = csvRubrics[3].concat(destCsv);
				}else{
					csvRubrics[1] = csvRubrics[1].concat(destCsv);
				}
			});
			// flatten
			csvRubrics = Array.prototype.concat.apply([], csvRubrics);

			// Make the content rows
			students.data.items.each(function(student){
				var csvStud = [student.get("id"), student.longName()];
				state.setStudent(student); // apply the filters

				// Collect the student scores into an array (same order as the header)
				rubrics.data.items.each(function(rubric){
					if (rubric.get("for_group")) return;
					var score = studentScores.findRecord("rubric_id", rubric.get("id"));
					if (score) {
						csvStud = csvStud.concat(score.get("integer_value"));
					}else{
						csvStud = csvStud.concat(0);
					}
				});

				// Collect the student's group scores.  Note that there is no part of the program preventing a student from being in multiple groups, so we will reflect this situation by adding multiple rows to the CSV.
				var multiCsvGrp = student.groups().filter(function(group){
					return group.get("module_number") === state.module.get("number");
				}).map(function(group){
					state.setGroup(group);
					var csvGrp = [];
					csvGrp = csvGrp.concat(group.get("recorded").toLocaleString());
					rubrics.data.items.each(function(rubric){
						if (!rubric.get("for_group")) return;
						var score = groupScores.findRecord("rubric_id", rubric.get("id"));
						if (score && score.get("integer_value")) {
							csvGrp = csvGrp.concat(score.get("integer_value"));
						}else{
							csvGrp = csvGrp.concat(0);
						}
					});
					return csvGrp;
				});

				// Add the group and student scores to the CSV
				csvData = csvData.concat(multiCsvGrp.map(function(csvGrp){
					return [].concat(csvStud, csvGrp);
				}));
			});

			// Compile the CSV
			var csvAll = [];
			csvAll.push(csvRubrics);
			csvAll = csvAll.concat(csvData);
			var csvStr = csvAll.map(function(csvRow){
				return '"' + csvRow.map(function(csvItem){
					return csvItem.toString().replace(/"/g, "\\\"");
				}).join('","') + '"';
			}).join("\n");

			// Download the CSV as a file (Chrome only)
			var csvDataString = encodeURI("data:text/csv;charset=utf-8," + csvStr);
			var linkObj = document.createElement("a");
			if (typeof linkObj.download != "undefined"){
				// use the <a download=""> attribute
				var fileName = "module" + state.module.get("number") + ".csv";
				linkObj.setAttribute("href", csvDataString);
				linkObj.setAttribute("download", fileName);
				linkObj.click();
			}else{
				// secondary option: download the data string without a file name
				window.location.href = csvDataString;
			}
		}
	};
})(window);