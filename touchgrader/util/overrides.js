// Production steps of ECMA-262, Edition 5, 15.4.4.19
// Reference: http://es5.github.com/#x15.4.4.19
Array.prototype.map||(Array.prototype.map=function(d,f){var g,e,a;if(null==this)throw new TypeError(" this is null or not defined");var b=Object(this),h=b.length>>>0;if("function"!==typeof d)throw new TypeError(d+" is not a function");f&&(g=f);e=Array(h);for(a=0;a<h;){var c;a in b&&(c=b[a],c=d.call(g,c,a,b),e[a]=c);a++}return e});

// Production steps of ECMA-262, Edition 5, 15.4.4.18
// Reference: http://es5.github.com/#x15.4.4.18
Array.prototype.forEach||(Array.prototype.forEach=function(c,f){var d,a;if(null==this)throw new TypeError("this is null or not defined");var b=Object(this),g=b.length>>>0;if("[object Function]"!=={}.toString.call(c))throw new TypeError(c+" is not a function");2<=arguments.length&&(d=f);for(a=0;a<g;){var e;a in b&&(e=b[a],c.call(d,e,a,b));a++}});

// Copy forEach() into each()
Array.prototype.each||(Array.prototype.each=Array.prototype.forEach);

// String.prototype.trim
String.prototype.trim||(String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"")});

// Array.prototype.filter
Array.prototype.filter||(Array.prototype.filter=function(c,f){if(null==this)throw new TypeError;var b=Object(this),g=b.length>>>0;if("function"!=typeof c)throw new TypeError;for(var d=[],a=0;a<g;a++)if(a in b){var e=b[a];c.call(f,e,a,b)&&d.push(e)}return d});

// Array.prototype.indexOf
Array.prototype.indexOf||(Array.prototype.indexOf=function(d){if(null==this)throw new TypeError;var c=Object(this),b=c.length>>>0;if(0===b)return-1;var a=0;1<arguments.length&&(a=Number(arguments[1]),a!=a?a=0:0!=a&&(Infinity!=a&&-Infinity!=a)&&(a=(0<a||-1)*Math.floor(Math.abs(a))));if(a>=b)return-1;for(a=0<=a?a:Math.max(b-Math.abs(a),0);a<b;a++)if(a in c&&c[a]===d)return a;return-1});

// Custom Array.prototype.findBy
Array.prototype.findBy || (Array.prototype.findBy = function(callback){
	if (this==null) throw new TypeError();
	for (var i=0; i<this.length; ++i){
		if (i in this && callback(this[i]) ){
			return this[i];
		}
	}
	return null;
});