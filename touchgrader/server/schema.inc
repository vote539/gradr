<?php

// Connect to the SQLite Database using PDO
$db = new PDO("sqlite:db/db.sqlite");
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Define the schema here.  This should closely reflect the schema in the Sencha Touch front end.
$schema = array(
	"student" => array(
		"id" => array("INTEGER", "NOT NULL PRIMARY KEY AUTOINCREMENT"),
		"first_name" => array("TEXT", "NOT NULL"),
		"last_name" => array("TEXT", "NOT NULL"),
		),
	"grp" => array(
		"id" => array("INTEGER", "NOT NULL PRIMARY KEY AUTOINCREMENT"),
		"recorded" => array("TEXT", "NOT NULL", "DEFAULT", "(date('now', 'localtime'))"), // date
		"module_number" => array("INTEGER", "NOT NULL"),
		),
	"students_in_groups" => array(
		"id" => array("INTEGER", "NOT NULL PRIMARY KEY AUTOINCREMENT"),
		"student_id" => array("INTEGER", "NOT NULL"),
		"group_id" => array("INTEGER", "NOT NULL"),
		),
	"module" => array(
		"id" => array("INTEGER", "NOT NULL PRIMARY KEY AUTOINCREMENT"),
		"number" => array("INTEGER", "NOT NULL"),
		),
	"rubric" => array(
		"id" => array("INTEGER", "NOT NULL PRIMARY KEY AUTOINCREMENT"),
		"type" => array("TEXT", "NOT NULL", "DEFAULT", "'range'"),
		"weight" => array("INTEGER", "NOT NULL"),
		"description" => array("TEXT", "NOT NULL"),
		"module_number" => array("INTEGER", "NOT NULL"),
		"for_group" => array("INTEGER", "NOT NULL", "DEFAULT", "1"), // boolean
		"special_instructions" => array("TEXT", ""),
		),
	"student_score" => array(
		"id" => array("INTEGER", "NOT NULL PRIMARY KEY AUTOINCREMENT"),
		"rubric_id" => array("INTEGER", "NOT NULL"),
		"integer_value" => array("INTEGER", ""),
		"string_value" => array("TEXT", ""),
		"student_id" => array("INTEGER", "NOT NULL"),
		),
	"group_score" => array(
		"id" => array("INTEGER", "NOT NULL PRIMARY KEY AUTOINCREMENT"),
		"rubric_id" => array("INTEGER", "NOT NULL"),
		"integer_value" => array("INTEGER", ""),
		"string_value" => array("TEXT", ""),
		"group_id" => array("INTEGER", "NOT NULL"),
		),
	"change_record" => array(
		"id" => array("INTEGER", "NOT NULL PRIMARY KEY AUTOINCREMENT"),
		"user" => array("TEXT", "NOT NULL"),
		"recorded" => array("DATETIME", "DEFAULT current_timestamp"),
		"query" => array("TEXT", "NOT NULL"),
		)
	);

// Create the schema if necessary
function db_make_schema(){
	global $db, $schema;
	foreach($schema as $table_name => $fields){
		$fields_strings = array();
		foreach($fields as $name => $properties){
			$fields_strings[] = $name . " " . implode(" ", $properties);
		}
		$q = "CREATE TABLE IF NOT EXISTS $table_name (" . implode(", ", $fields_strings) . ")";
		try{
			$db->exec($q);
		}catch(PDOException $e){
			error_log($q);
			throw $e;
		}
	}
}

// Get all data from a database with the correct datatypes
function db_get_all($table){
	global $db;

	// Query the database
	$q = "SELECT * FROM $table";
	$res = $db->query($q);
	$rows = $res->fetchAll(PDO::FETCH_ASSOC);

	// Cast the data types
	db_cast($table, $rows);
	
	// Return data
	return $rows;
}

// Cast data types to an array of rows
function db_cast($table, &$rows){
	global $schema;
	foreach($rows as $rowk => $rowv){
		foreach($rowv as $fk => $fv){
			if ($fk == "clientId") continue;
			$sql_type = $schema[$table][$fk][0];
			if ($sql_type == "INTEGER"){
				$rows[$rowk][$fk] += 0; // cast to integer
			}
		}
	}
}

// Create a row in the database with the specified data
function db_create($table, $data){
	global $db, $schema;

	// Prepare the statement
	// Slice the fields to get rid of the ID
	$field_names = array_slice(array_keys($schema[$table]), 1);
	$filled_table_array = array_fill(0, count($field_names), $table);
	$insert_q = sprintf("INSERT INTO $table (%s) VALUES (%s)",
		implode(", ", $field_names),
		implode(", ", array_map("coalesce", $field_names, $filled_table_array))
		);
	try{
		$stmt = $db->prepare($insert_q);
	}catch(PDOException $e){
		error_log($insert_q);
		throw $e;
	}
	$object = array();
	foreach($field_names as $field){
		$fval = @$data[$field];
		$fval_param = add_colon($field);

		if(empty($fval)){
			$stmt->bindValue($fval_param, null, PDO::PARAM_NULL);
		}else{
			$stmt->bindValue($fval_param, $fval);
		}
		$object[$field] = @$data[$field];
	}

	// Execute the statement
	$stmt->execute();

	// Prepare the data for display (get the new ID and cast data types)
	$object["id"] = $db->lastInsertId();
	$object["clientId"] = $data["id"];
	$obj_arr = array();
	array_push($obj_arr, $object);
	db_cast($table, $obj_arr);

	// Record activity
	db_record_activity($insert_q, $object);

	// Return the data
	return $obj_arr;
}

// Update a row in the database with the specified data
function db_update($table, $data){
	global $db, $schema;

	// Prepare the statement
	// Slice the fields to get rid of the ID
	$field_names = array_slice(array_keys($schema[$table]), 1);
	$update_q = sprintf("UPDATE $table SET %s WHERE id=:id",
		implode(", ", array_map("pairwise_assignment", $field_names))
		);
	$stmt = $db->prepare($update_q);
	$object = array();
	foreach($field_names as $field){
		$stmt->bindValue(add_colon($field), @$data[$field]);
		$object[$field] = @$data[$field];
	}

	// Bind the ID
	$stmt->bindValue(":id", $data["id"]);
	$object["id"] = (int) $data["id"];

	// Execute the statement
	$stmt->execute();

	// Prepare the data for display
	$obj_arr = array();
	array_push($obj_arr, $object);
	db_cast($table, $obj_arr);

	// Record activity
	db_record_activity($update_q, $object);

	// Return the data
	return $obj_arr;
}

// Record database activity (for accountability)
function db_record_activity($query, $object){
	global $db;
	// HTTP Basic Auth User
	$user = $_SERVER["PHP_AUTH_USER"];
	$interpolated_query = $query;
	foreach($object as $k => $v){
		$interpolated_query = str_replace(":$k",
			"'".addslashes($v)."'", $interpolated_query);
	}

	$activity_q = "INSERT INTO change_record (user, query) VALUES (:user, :query)";
	$stmt = $db->prepare($activity_q);
	$stmt->bindValue(":user", $user);
	$stmt->bindValue(":query", $interpolated_query);
	$stmt->execute();
}

// Add a colon to an array of strings.  Used to make interpolation-friendly prepared queries
function add_colon($field){
	return ":$field";
}

// Make a coalesce relationship to enable default values to be specified for null fields
function coalesce($field, $table){
	global $schema;
	$fdefault = @$schema[$table][$field][3];
	if(!empty($fdefault)){
		return "coalesce(:$field, ".$fdefault.")";
	}else{
		return ":$field";
	}
}

// Make pairwise assignment operators.  Used to make interpolation-friendly update queries.
function pairwise_assignment($field){
	return "$field=:$field";
}

?>
