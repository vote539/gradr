<?php

// Controller
function control_get(){
	$table = route_to_table($_REQUEST["uri"]); // will be sql-safe
	return json(db_get_all($table));
}

function control_post(){
	$table = route_to_table($_REQUEST["uri"]);
	$data = control_get_json();

	// RESTful routes don't work too well in PHP.
	// Had to revert back to POST routes for everything.
	if (is_numeric(@$data["id"]) || is_numeric(@$data[0]["id"])){
		return control_put($table, $data);
	}

	// Pass the information down to schema.inc
	if(!isset($data["id"])){
		$output = array();
		foreach($data as $datum){
			$output = array_merge($output, db_create($table, $datum));
		}
	}else{
		$output = db_create($table, $data);
	}
	return json($output);
}

function control_put($table, $data){
	if (empty($table)){
		$table = route_to_table($_REQUEST["uri"]);
		$data = control_get_json();
	}

	// Pass the information down to schema.inc
	if(!isset($data["id"])){
		$output = array();
		foreach($data as $datum){
			$output = array_merge($output, db_update($table, $datum));
		}
	}else{
		$output = db_update($table, $data);
	}
	return json($output);
}

function control_get_json(){
	if (!empty($_POST)) return $_POST;
	// Parse the JSON input from Sencha Touch
	$raw = file_get_contents("php://input");
	$data = json_decode($raw);
	if (is_array($data)){
		foreach($data as $k => $v){
			$data[$k] = get_object_vars($v);
		}
	}else{
		$data = get_object_vars($data);
	}
	return $data;
}

?>
