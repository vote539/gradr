/*
 * File: app/view/GroupView.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Gradr.view.GroupView', {
    extend: 'Ext.Container',
    alias: 'widget.groupview',

    config: {
        itemId: 'GroupView',
        ui: '',
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'button',
                itemId: 'addStudentBtn',
                margin: '20px',
                ui: 'round',
                text: 'Add Student…'
            },
            {
                xtype: 'list',
                flex: 1,
                itemId: 'currentStudentsList',
                ui: 'round',
                emptyText: 'No Students',
                itemTpl: Ext.create('Ext.XTemplate', 
                    '{[this.fullName(values.id)]}',
                    {
                        fullName: function(id) {
                            return Ext.getStore("Students").getById(id).longName();
                        }
                    }
                ),
                store: 'StudentsInCurrentGroup',
                onItemDisclosure: true
            },
            {
                xtype: 'datepickerfield',
                height: '47px',
                itemId: 'SubmissionDatePicker',
                margin: '0 5%',
                label: 'Date',
                placeHolder: 'mm/dd/yyyy'
            },
            {
                xtype: 'button',
                height: '34px',
                itemId: 'ScoreGroupBtn',
                margin: '20px',
                ui: 'action-round',
                text: 'Score Group'
            }
        ],
        listeners: [
            {
                fn: 'onAddStudentBtnTap',
                event: 'tap',
                delegate: '#addStudentBtn'
            },
            {
                fn: 'onCurrentStudentsListDisclose',
                event: 'disclose',
                delegate: '#currentStudentsList'
            },
            {
                fn: 'onSubmissionDatePickerChange',
                event: 'change',
                delegate: '#SubmissionDatePicker'
            },
            {
                fn: 'onScoreGroupBtnTap',
                event: 'tap',
                delegate: '#ScoreGroupBtn'
            }
        ]
    },

    onAddStudentBtnTap: function(button, e, eOpts) {
        State.showStudentChooser(button.up("applicationview"));
    },

    onCurrentStudentsListDisclose: function(list, record, target, index, e, eOpts) {
        State.setStudent(record);
        State.showGradeStudent(list.up("applicationview"));
    },

    onSubmissionDatePickerChange: function(datepickerfield, newDate, oldDate, eOpts) {
        State.updateGroupSubmissionDate(newDate);
    },

    onScoreGroupBtnTap: function(button, e, eOpts) {
        State.showGradeGroup(button.up("applicationview"));
    },

    initialize: function() {
        this.callParent();
        this.config.title = State.group.titleSafe();

        var displayedDate = State.group.get("recorded") || new Date();
        var datePicker = this.down("#SubmissionDatePicker");
        datePicker.setValue(displayedDate);
    }

});