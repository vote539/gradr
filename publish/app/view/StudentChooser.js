/*
 * File: app/view/StudentChooser.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Gradr.view.StudentChooser', {
    extend: 'Ext.Container',
    alias: 'widget.studentchooser',

    config: {
        title: 'Add Student',
        itemId: 'StudentChooser',
        layout: {
            type: 'fit'
        },
        items: [
            {
                xtype: 'list',
                itemId: 'StudentChooserList',
                itemTpl: Ext.create('Ext.XTemplate', 
                    '{[this.fullName(values.id)]}',
                    {
                        fullName: function(id) {
                            return Ext.getStore("Students").getById(id).longName();
                        }
                    }
                ),
                store: 'Students',
                grouped: true,
                indexBar: true
            }
        ],
        listeners: [
            {
                fn: 'onStudentChooserListItemTap',
                event: 'itemtap',
                delegate: '#StudentChooserList'
            }
        ]
    },

    onStudentChooserListItemTap: function(dataview, index, target, record, e, eOpts) {
        var applicationView = dataview.up("applicationview");
        State.addStudent(record);
        applicationView.pop();

        // Update the title on the group view
        applicationView.getNavigationBar().setTitle(State.group.titleSafe());
    }

});