<?php

// Map routes to tables
$routes_to_tables = array(
	"students" => "student",
	"groups" => "grp",
	"sg" => "students_in_groups",
	"modules" => "module",
	"rubrics" => "rubric",
	"ss" => "student_score",
	"gs" => "group_score"
);
function route_to_table($route){
	global $routes_to_tables;
	// get only the part of the URL before any slashes
	preg_match("/^\w+/", $route, $matches);
	return $routes_to_tables[$matches[0]];
}

// Make the routes based on the map
foreach($routes_to_tables as $route => $table){
	dispatch_get("$route", "control_get");
	dispatch_post("$route", "control_post");
}

?>