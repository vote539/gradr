<?php

// Require the Limonade library
require_once "../../limonade/lib/limonade.php";

// Require local libraries
require_once "json.inc"; // required for PHP 5.1
require_once "schema.inc";
db_make_schema();
require_once "routes.inc";
require_once "controller.inc";

// Run Limonade
run();

?>