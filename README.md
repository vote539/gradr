Gradr: A Mobile Student Grading System
======================================

Need a modern, mobile grading system for your lab-based classes to use on Demo Day?  Then **Gradr** might be for you.

Built using [Sencha Touch](http://www.sencha.com/products/touch), Gradr uses the latest Web technologies to provide a fully **cross-platform** mobile web app.  Gradr works great on iPhone, Android, BlackBerry 10, Windows Phone, and other modern web platforms.

Stuck with an old server that your institution's IT department hasn't upgraded for five years?  Don't have access to a database?  No problem!  Gradr uses a **PHP and SQLite** back-end that works in PHP 5.1 and higher (released in 2007).

## Features

- Create one or more *Modules* for each demo day.
- Grade students in *Groups*.
- Enable both Individual Scores and Group Scores.
- Itemize grades using a *Rubric*.

## Installation

1. Upload the `touchgrader/build/touchgrader/production` directory (aka, the *package* directory) to your server.  You may rename this directory if you like.
2. Upload the following files and directories from `touchgrader` to your *package* directory:
	- `server/*`
	- `util/*`
	- `.htaccess`
3. [Make a `.htpasswd` file](http://aspirine.org/htpasswd_en.html) and upload it to your *package* directory.  The usernames and passwords specified here will be used by your instructors and teaching assistants.
4. Edit the `.htaccess` file you uploaded and on line 3, specify the absolute path to the `.htpasswd` file you uploaded above.  (Sorry, but Apache doesn't provide an easier way to do this.)
5. Set the following UNIX permissions:
	- On the `server/db` directory: 777
	- On the `server/db/db.sqlite` binary database file: 777
6. Open `server/adminer.php` in your web browser.  You will need to enter a username and password corresponding to an entry in your `.htpasswd` file.  Enter `db/db.sqlite` as the database name.  If everything is working you should see something like this: ![Adminer Home Screen](http://i39.tinypic.com/34pdy0m.png)
7. Open the `student` table and import your students by their Student ID, First Name, and Last Name.  We recommend making a CSV file with three columns, and then importing the CSV by going to *select data -> import* in Adminer.  *Note:* After you import the students, all other operations in Gradr, including the creation of rubrics, can be done through the GUI.  You may not need to open the Adminer interface again.
8. You should now be all set to go.  Open `index.html` in your browser to launch the mobile grading interface.

## Instructor Controls

Use the *Instructor Controls* to create new modules and add rubric items to existing modules.

1. On the home screen of the mobile interface, click "Instructor Controls"
2. Enter a new module number on the top and click "New Module"
3. Add rubric items to this module.  You can either create the rubric items individually by clicking "New Item" or you can import a CSV-style list of rubric items by clicking "Import Items".

## Grading Controls

On the home screen, tap a module by the arrow to open that module's grading center.  Tap an existing group to change that group's grade or the grades of the students in that group.  Tap "New Group" to create an empty group.  When in a group view, tapp "Add Student" to add a student to the group.  To grade an individual student, tap the arrow by their name in the group view.  To grade a group, tap "Score Group" at the bottom of the group view.  You may optionally specify a date that the group grade was recorded.

## Notes

### Accountability

All queries that are performed on the database via the mobile front-end are recorded in the `change_record` table in the database, along with the user who performed the action (the username indirectly comes from `.htpasswd`).

## License

This Source Code is subject to the terms of the [Mozilla Public License 2.0](http://mozilla.org/MPL/2.0/).  [Mozilla Public License 2.0 Summary](http://www.tldrlegal.com/l/MPL2).